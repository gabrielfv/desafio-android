package com.gabrielfv.desafioconcrete.app.di

import android.app.Application
import com.gabrielfv.desafioconcrete.api.di.ApiModule
import com.gabrielfv.desafioconcrete.app.App
import com.gabrielfv.desafioconcrete.ui.di.UiModule
import com.gabrielfv.desafioconcrete.ui.pulls.PullViewModule
import com.gabrielfv.desafioconcrete.ui.repos.RepoViewModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule

@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    ApiModule::class,
    UiModule::class,
    RepoViewModule::class,
    PullViewModule::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)
}