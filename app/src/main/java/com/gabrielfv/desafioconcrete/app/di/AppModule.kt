package com.gabrielfv.desafioconcrete.app.di

import com.gabrielfv.desafioconcrete.app.MainScheduleProvider
import com.gabrielfv.desafioconcrete.app.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideScheduler(): SchedulerProvider = MainScheduleProvider()
}