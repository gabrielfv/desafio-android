package com.gabrielfv.desafioconcrete.ui.pulls

import android.content.Context
import com.gabrielfv.desafioconcrete.api.Constants
import com.gabrielfv.desafioconcrete.api.GithubApi
import com.gabrielfv.desafioconcrete.api.models.Pull
import com.gabrielfv.desafioconcrete.api.models.Repo
import com.gabrielfv.desafioconcrete.app.SchedulerProvider
import com.gabrielfv.desafioconcrete.ui.BaseViewModel
import io.reactivex.exceptions.CompositeException
import javax.inject.Inject

class PullViewModel @Inject constructor(
        val api: GithubApi,
        val scheduleProvider: SchedulerProvider
) : BaseViewModel<Pull>() {
    lateinit var repo: Repo

    fun init(repo: Repo) {
        this.repo = repo
    }

    override fun fetchDataInternal() {
        val preSize = data.size
        api.fetchRepoPulls(repo, page)
                ?.observeOn(scheduleProvider.ui())
                ?.subscribe({ pull ->
                    data.add(pull)
                }, { e ->
                    if (data.isEmpty()) {
                        onError?.invoke(CompositeException(
                                e,
                                NoDataFetchedException("Couldn't fetch data")
                        ))
                    } else onError?.invoke(e)
                }, {
                    if (data.isEmpty()) {
                        onError?.invoke(NoDataFetchedException("API Returned no data"))
                    } else {
                        if (data.size > preSize) onDataSetChanged?.invoke()
                        if ((data.size - preSize) < Constants.GITHUB_PAGE_SIZE) {
                            onCompletedListener?.invoke()
                        } else {
                            this.page++
                        }
                    }
                })
    }

    fun subscribeAdapter(context: Context) = PullAdapter(data, context)
}