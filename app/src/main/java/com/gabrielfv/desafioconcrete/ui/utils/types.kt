package com.gabrielfv.desafioconcrete.ui.utils

typealias OnDataSetChangedListener = () -> Unit
typealias OnErrorListener = (Throwable) -> Unit
typealias OnCompletedListener = () -> Unit
typealias OnDataSetLoadingListener = () -> Unit