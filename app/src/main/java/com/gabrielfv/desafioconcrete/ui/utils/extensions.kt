package com.gabrielfv.desafioconcrete.ui.utils

import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.gabrielfv.desafioconcrete.R

fun AppCompatActivity.showAllertSnackbar(view: View, msg: String) {
    val snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
    snackbar.setAction("DISMISS") { _ ->
        snackbar.dismiss()
    }
    snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.allertColor))
    val textView: TextView = snackbar.view.findViewById(android.support.design.R.id.snackbar_text)
    textView.setTextColor(ContextCompat.getColor(this, android.R.color.black))
    snackbar.show()
}

fun <T> Boolean.then(block: () -> T): T? {
    return if (this) block()
    else null
}
