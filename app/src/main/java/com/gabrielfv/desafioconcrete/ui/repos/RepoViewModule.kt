package com.gabrielfv.desafioconcrete.ui.repos

import android.app.Activity
import android.arch.lifecycle.ViewModel
import com.gabrielfv.desafioconcrete.app.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

@Module(subcomponents = [RepoViewSubcomponent::class])
abstract class RepoViewModule {

    @Binds
    @IntoMap
    @ActivityKey(RepoActivity::class)
    abstract fun bindAndroidInjectorFactory(
            builder: RepoViewSubcomponent.Builder
    ): AndroidInjector.Factory<out Activity>

    @Binds
    @IntoMap
    @ViewModelKey(RepoViewModel::class)
    abstract fun bindRepoViewModel(viewModel: RepoViewModel): ViewModel
}