package com.gabrielfv.desafioconcrete.ui.pulls

import android.app.Activity
import android.arch.lifecycle.ViewModel
import com.gabrielfv.desafioconcrete.app.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

@Module(subcomponents = [PullViewSubcomponent::class])
abstract class PullViewModule {

    @Binds
    @IntoMap
    @ActivityKey(PullActivity::class)
    abstract fun bindAndroidInjectorFactory(
            builder: PullViewSubcomponent.Builder
    ): AndroidInjector.Factory<out Activity>

    @Binds
    @IntoMap
    @ViewModelKey(PullViewModel::class)
    abstract fun bindRepoViewModel(viewModel: PullViewModel): ViewModel
}