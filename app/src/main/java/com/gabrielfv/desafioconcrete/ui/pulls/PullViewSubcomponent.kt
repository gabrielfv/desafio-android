package com.gabrielfv.desafioconcrete.ui.pulls

import com.gabrielfv.desafioconcrete.api.di.ApiModule
import com.gabrielfv.desafioconcrete.app.di.AppModule
import com.gabrielfv.desafioconcrete.ui.di.UiModule
import dagger.Subcomponent
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Subcomponent(modules = [AppModule::class, ApiModule::class, UiModule::class])
interface PullViewSubcomponent : AndroidInjector<PullActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<PullActivity>() {}
}