package com.gabrielfv.desafioconcrete.ui.utils

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import kotlin.math.max

abstract class EndlessScrollListener(
        private val layoutManager: RecyclerView.LayoutManager,
        private var threshold: Int = 3
) : RecyclerView.OnScrollListener() {
    private var previousTotalCount: Int = 0
    private var loading: Boolean = true

    init {
        threshold = when (layoutManager) {
            is GridLayoutManager -> threshold * layoutManager.spanCount
            is StaggeredGridLayoutManager -> threshold * layoutManager.spanCount
            is LinearLayoutManager -> threshold
            else -> threshold
        }
    }

    fun staggeredGridLastVisibleItem(lastVisiblePositions: IntArray): Int {
        return lastVisiblePositions.fold(-1, ::max)
    }

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        val totalCount = layoutManager.itemCount
        val lastPos: Int = when(layoutManager) {
            is StaggeredGridLayoutManager -> staggeredGridLastVisibleItem(
                    layoutManager.findLastCompletelyVisibleItemPositions(null)
            )
            is GridLayoutManager -> layoutManager.findLastVisibleItemPosition()
            is LinearLayoutManager -> layoutManager.findLastVisibleItemPosition()
            else -> throw NotImplementedError("Can't fetch last position for unknown layout manager")
        }

        if (totalCount < previousTotalCount) {
            previousTotalCount = totalCount
            if (totalCount == 0) loading = true
        }

        if (loading && (totalCount > previousTotalCount)) {
            loading = false
            previousTotalCount = totalCount
        }

        if (!loading && (lastPos + threshold) > totalCount) {
            onLoadMore()
            loading = true
        }
    }

    abstract fun onLoadMore()
}