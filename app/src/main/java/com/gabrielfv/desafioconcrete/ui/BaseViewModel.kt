package com.gabrielfv.desafioconcrete.ui

import android.arch.lifecycle.ViewModel
import com.gabrielfv.desafioconcrete.ui.utils.OnCompletedListener
import com.gabrielfv.desafioconcrete.ui.utils.OnDataSetChangedListener
import com.gabrielfv.desafioconcrete.ui.utils.OnDataSetLoadingListener
import com.gabrielfv.desafioconcrete.ui.utils.OnErrorListener

abstract class BaseViewModel<T> : ViewModel() {
    class NoDataFetchedException(msg: String?) : Exception(msg)

    val data: ArrayList<T> = ArrayList()
    var page: Int = 1
        protected set

    protected var onDataSetChanged: OnDataSetChangedListener? = null
    protected var onError: OnErrorListener? = null
    protected var onCompletedListener: OnCompletedListener? = null
    protected var onDataSetLoading: OnDataSetLoadingListener? = null

    fun subscribe(
            onDataSetChanged: OnDataSetChangedListener,
            onError: OnErrorListener? = null,
            onCompletedListener: OnCompletedListener? = null,
            onDataSetLoadingListener: OnDataSetLoadingListener? = null) {
        this.onDataSetChanged = onDataSetChanged
        this.onError = onError
        this.onCompletedListener = onCompletedListener
        this.onDataSetLoading = onDataSetLoadingListener
        if (data.isEmpty()) {
            this.onDataSetLoading?.invoke()
            fetchData()
        }
    }

    fun fetchData() {
        this.onDataSetLoading?.invoke()
        fetchDataInternal()
    }

    protected abstract fun fetchDataInternal()
}