package com.gabrielfv.desafioconcrete.ui.pulls

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.gabrielfv.desafioconcrete.R
import com.gabrielfv.desafioconcrete.api.models.Repo
import com.gabrielfv.desafioconcrete.ui.BaseViewModel
import com.gabrielfv.desafioconcrete.ui.utils.EndlessScrollListener
import com.gabrielfv.desafioconcrete.ui.utils.showAllertSnackbar
import com.gabrielfv.desafioconcrete.ui.utils.then
import com.squareup.picasso.Picasso
import dagger.android.AndroidInjection
import io.reactivex.exceptions.CompositeException
import kotlinx.android.synthetic.main.actionbar_pull.*
import kotlinx.android.synthetic.main.activity_pull.*
import timber.log.Timber
import javax.inject.Inject

class PullActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: PullViewModel

    private lateinit var adapter: PullAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull)

        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(PullViewModel::class.java)

        init(intent.getParcelableExtra(EXTRA_REPO))

        adapter = viewModel.subscribeAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        recyclerView.addOnScrollListener(object : EndlessScrollListener(
                recyclerView.layoutManager, 5) {
            override fun onLoadMore() {
                viewModel.fetchData()
            }
        })

        viewModel.subscribe({
            hideLoadingIndicator()
            adapter.notifyDataSetChanged()
            if (recyclerView.visibility == View.GONE) noDataHide()
        }, { e ->
            hideLoadingIndicator()
            Timber.e(e)
            if (e !is BaseViewModel.NoDataFetchedException) {
                showAllertSnackbar(
                        recyclerView,
                        getString(R.string.data_fetch_error_format).format("repos")
                )
                if (e is CompositeException) {
                    e.exceptions.any { t -> t is BaseViewModel.NoDataFetchedException }. then {
                        noDataDisplay()
                    }
                }
            } else noDataDisplay()
        }, {
            recyclerView.clearOnScrollListeners()
        }, {
            displayLoadingIndicator()
        })
    }

    private fun init(repo: Repo) {
        viewModel.init(repo)
        repoName.text = repo.fullName
        backButton.setOnClickListener { _ ->
            finish()
        }
        Picasso.get()
                .load(repo.owner.avatarUrl)
                .placeholder(R.mipmap.ic_launcher)
                .into(repoOwnerAvatar)
    }

    private fun displayLoadingIndicator() {
        loadingIndicator.visibility = View.VISIBLE
    }

    private fun hideLoadingIndicator() {
        loadingIndicator.visibility = View.GONE
    }

    private fun noDataDisplay() {
        recyclerView.visibility = View.GONE
        noDataImage.visibility = View.VISIBLE
        noDataText.visibility = View.VISIBLE
    }

    private fun noDataHide() {
        recyclerView.visibility = View.VISIBLE
        noDataImage.visibility = View.GONE
        noDataText.visibility = View.GONE
    }

    companion object {
        const val EXTRA_REPO = "com.gabrielfv.desafioconcrete.EXTRA_REPO"
    }
}
