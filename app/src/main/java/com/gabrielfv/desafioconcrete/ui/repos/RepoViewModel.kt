package com.gabrielfv.desafioconcrete.ui.repos

import android.content.Context
import com.gabrielfv.desafioconcrete.api.Constants
import com.gabrielfv.desafioconcrete.api.GithubApi
import com.gabrielfv.desafioconcrete.api.GithubSearchQuery
import com.gabrielfv.desafioconcrete.api.models.Repo
import com.gabrielfv.desafioconcrete.app.SchedulerProvider
import com.gabrielfv.desafioconcrete.ui.BaseViewModel
import io.reactivex.exceptions.CompositeException
import javax.inject.Inject

class RepoViewModel @Inject constructor(
        private val api: GithubApi,
        private val schedulerProvider: SchedulerProvider
): BaseViewModel<Repo>() {

    override fun fetchDataInternal() {
        val query = GithubSearchQuery.Builder()
                .setQuery("language", "java")
                .build()
        val preSize = data.size
        api.fetchRepos(query, GithubApi.OrderBy.DESC, GithubApi.SortBy.STARS, page)
                ?.observeOn(schedulerProvider.ui())
                ?.subscribe({ repo ->
                    data.add(repo)
                }, { e ->
                    if (data.isEmpty()) {
                        onError?.invoke(CompositeException(
                                e,
                                NoDataFetchedException("Couldn't fetch data")
                        ))
                    } else onError?.invoke(e)
                }, {
                    if (data.isEmpty()) {
                        onError?.invoke(NoDataFetchedException("API Returned no data"))
                    } else {
                        if (data.size > preSize) onDataSetChanged?.invoke()
                        if ((data.size - preSize) < Constants.GITHUB_PAGE_SIZE) {
                            onCompletedListener?.invoke()
                        } else {
                            this.page++
                        }
                    }
                })
    }

    fun subscribeAdapter(context: Context) = RepoAdapter(data, context)
}
