package com.gabrielfv.desafioconcrete.ui.utils

import java.text.SimpleDateFormat
import java.util.*

class TimeAgo {
    companion object {
        private fun fromDate(date: Date, withYear: Boolean = false): String {
            val pattern = "MMM d${if (withYear) ", yyyy" else ""}"
            val df = SimpleDateFormat(pattern, Locale.getDefault())
            df.timeZone = TimeZone.getDefault()
            return df.format(date)
        }

        fun getTimeAgo(then: Date): String {
            val diff = Date().time - then.time

            (diff / 1000).let { seconds ->
                if (seconds < 60) return "${seconds}s ago"
                seconds / 60
            }.let { minutes ->
                if (minutes < 60) return "${minutes}min ago"
                minutes / 60
            }.let { hours ->
                if (hours < 24) return "${hours}h ago"
                hours / 24
            }.let { days ->
                if (days < 7) return "$days day${if (days > 1) "s" else ""} ago"
                else if (days < 365) return fromDate(then)
                else return fromDate(then, true)
            }
        }
    }
}