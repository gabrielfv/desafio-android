package com.gabrielfv.desafioconcrete.ui.repos

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.gabrielfv.desafioconcrete.R
import com.gabrielfv.desafioconcrete.ui.BaseViewModel
import com.gabrielfv.desafioconcrete.ui.utils.EndlessScrollListener
import com.gabrielfv.desafioconcrete.ui.utils.showAllertSnackbar
import com.gabrielfv.desafioconcrete.ui.utils.then
import dagger.android.AndroidInjection
import io.reactivex.exceptions.CompositeException
import kotlinx.android.synthetic.main.activity_repo.*
import timber.log.Timber
import javax.inject.Inject

class RepoActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: RepoViewModel

    private lateinit var adapter: RepoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo)

        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(RepoViewModel::class.java)

        adapter = viewModel.subscribeAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addOnScrollListener(object : EndlessScrollListener(
                recyclerView.layoutManager, 5) {
            override fun onLoadMore() {
                viewModel.fetchData()
            }
        })

        viewModel.subscribe({
            hideLoadingIndicator()
            if (recyclerView.visibility == View.GONE) noDataHide()
            adapter.notifyDataSetChanged()
        }, { e ->
            hideLoadingIndicator()
            Timber.e(e)
            if (e !is BaseViewModel.NoDataFetchedException) {
                showAllertSnackbar(
                        recyclerView,
                        getString(R.string.data_fetch_error_format).format("repos")
                )
                if (e is CompositeException) {
                    e.exceptions.any { t -> t is BaseViewModel.NoDataFetchedException }. then {
                        noDataDisplay()
                    }
                }
            } else noDataDisplay()
        }, {
            recyclerView.clearOnScrollListeners()
        }, {
            displayLoadingIndicator()
        })
    }

    private fun displayLoadingIndicator() {
        loadingIndicator.visibility = View.VISIBLE
    }

    private fun hideLoadingIndicator() {
        loadingIndicator.visibility = View.GONE
    }

    private fun noDataDisplay() {
        recyclerView.visibility = View.GONE
        noDataImage.visibility = View.VISIBLE
        noDataText.visibility = View.VISIBLE
    }

    private fun noDataHide() {
        recyclerView.visibility = View.VISIBLE
        noDataImage.visibility = View.GONE
        noDataText.visibility = View.GONE
    }
}
