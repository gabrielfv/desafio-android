package com.gabrielfv.desafioconcrete.ui.pulls

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gabrielfv.desafioconcrete.R
import com.gabrielfv.desafioconcrete.api.models.Pull
import com.gabrielfv.desafioconcrete.ui.utils.TimeAgo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_pull.view.*

class PullAdapter(
        val data: List<Pull>,
        val context: Context
): RecyclerView.Adapter<PullAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(context).inflate(R.layout.adapter_pull, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pull = data[position]
        holder.title.text = pull.title
        holder.body.text = pull.body
        holder.user.text = context.getString(R.string.credits_format)
                .format(pull.user.login)
        holder.createdAt.text = context.getString(R.string.created_at_format)
                .format(TimeAgo.getTimeAgo(pull.createdAt))

        holder.updatedAt.text = if (pull.updatedAt > pull.createdAt) {
            context.getString(R.string.updated_at_format)
                    .format(TimeAgo.getTimeAgo(pull.updatedAt))
        } else ""

        Picasso.get()
                .load(pull.user.avatarUrl)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.authorAvatar)
    }

    override fun getItemCount(): Int = data.size

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val title = itemView.title
        val body = itemView.body
        val user = itemView.user
        val createdAt = itemView.createdAt
        val updatedAt = itemView.updatedAt
        val authorAvatar = itemView.authorAvatar
    }
}