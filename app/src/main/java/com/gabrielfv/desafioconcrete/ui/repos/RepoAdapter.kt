package com.gabrielfv.desafioconcrete.ui.repos

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gabrielfv.desafioconcrete.R
import com.gabrielfv.desafioconcrete.api.models.Repo
import com.gabrielfv.desafioconcrete.ui.pulls.PullActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_repo.view.*

class RepoAdapter(
        val data: List<Repo>,
        private val context: Context
) : RecyclerView.Adapter<RepoAdapter.ViewHolder>() {
    private lateinit var recyclerView: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
                .from(context)
                .inflate(R.layout.adapter_repo, parent, false)
        view.setOnClickListener { v ->
            val i = Intent(context, PullActivity::class.java)
            i.putExtra(PullActivity.EXTRA_REPO, data.get(recyclerView.getChildLayoutPosition(v)))
            context.startActivity(i)
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repo = data[position]
        holder.repoName.text = repo.name
        holder.forkCount.text = repo.forksCount.toString()
        holder.starCount.text = repo.stargazersCount.toString()
        holder.description.text = repo.description
        holder.ownerName.text = context.getString(R.string.credits_format)
                .format(repo.owner.login)
        Picasso.get()
                .load(repo.owner.avatarUrl)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.ownerAvatar)
    }

    override fun getItemCount(): Int = data.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val repoName = itemView.repoName
        val ownerName = itemView.ownerName
        val starCount = itemView.starCount
        val forkCount = itemView.forkCount
        val ownerAvatar = itemView.ownerAvatar
        val description = itemView.description
    }
}