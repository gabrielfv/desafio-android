package com.gabrielfv.desafioconcrete.api

import com.gabrielfv.desafioconcrete.api.models.ApiResponse
import com.gabrielfv.desafioconcrete.api.models.Pull
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubApiService {
    @GET(Constants.GITHUB_API_REPO_SEARCH)
    fun searchRepos(
            @Query("q") query: String,
            @Query("sort") sortBy: String? = null,
            @Query("order") order: String? = null,
            @Query("page") page: Int? = null
    ): Observable<ApiResponse>?

    @GET(Constants.GITHUB_API_PULLS)
    fun getRepoPulls(
            @Path("owner") owner: String,
            @Path("name") name: String,
            @Query("page") page: Int = 1
    ): Observable<List<Pull>>?
}