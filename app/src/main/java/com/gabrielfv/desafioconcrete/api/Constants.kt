package com.gabrielfv.desafioconcrete.api

class Constants {
    companion object {
        const val GITHUB_API_BASE_URL = "https://api.github.com/"
        const val GITHUB_API_REPO_SEARCH = "search/repositories"
        const val GITHUB_API_PULLS = "repos/{owner}/{name}/pulls"
        const val CACHE_SIZE = 8 * 1024 * 1024L
        const val GITHUB_PAGE_SIZE = 30
    }
}