package com.gabrielfv.desafioconcrete.api

import com.gabrielfv.desafioconcrete.api.GithubSearchQuery.Relation.Companion.EQUAL
import com.gabrielfv.desafioconcrete.api.GithubSearchQuery.Relation.Companion.GREATER_THEN
import com.gabrielfv.desafioconcrete.api.GithubSearchQuery.Relation.Companion.LOWER_THEN

class GithubSearchQuery(private val pattern: String = "") {
    private val queries: HashMap<String, String> by lazy { HashMap<String, String>() }

    class Relation private constructor(private val bit: Int) {
        companion object {
            val EQUAL = Relation(1 shl 0)
            val GREATER_THEN = Relation(1 shl 1)
            val LOWER_THEN = Relation(1 shl 2)
        }

        fun toInt(): Int = bit

        infix fun and(other: Relation): Int = this.toInt() and other.toInt()

        infix fun or(other: Relation): Relation = Relation(this.toInt() or other.toInt())

        operator fun plus(other: Relation): Relation = Relation(this.toInt() + other.toInt())
    }

    class Builder constructor(private val pattern: String = "") {
        private val queryObj: GithubSearchQuery by lazy { GithubSearchQuery(pattern) }

        fun setQuery(query: String, value: String, exclude: Boolean = false): Builder {
            queryObj.setQuery(query, value, exclude)
            return this
        }

        fun setRelationQuery(query: String, value: Int, flags: Relation = EQUAL): Builder {
            queryObj.setRelationQuery(query, value, flags)
            return this
        }

        fun setRangeQuery(query: String, lower: Int, upper: Int): Builder {
            queryObj.setRangeQuery(query, lower, upper)
            return this
        }

        fun build(): GithubSearchQuery = queryObj
    }

    fun setQuery(query: String, value: String, exclude: Boolean = false) {
        queries["${if (exclude) "-" else ""}$query"] = value
    }

    fun setRelationQuery(query: String, value: Int, flags: Relation = EQUAL) {
        if ((flags and GREATER_THEN != 0) && (flags and LOWER_THEN != 0)) {
            throw IllegalArgumentException()
        }

        queries[query] = if (flags and EQUAL != 0) {
            if (flags and GREATER_THEN != 0) ">=$value"
            else if (flags and LOWER_THEN != 0) "<=$value"
            else "$value"
        } else {
            if (flags and GREATER_THEN != 0) ">$value"
            else if (flags and LOWER_THEN != 0) "<$value"
            else throw IllegalArgumentException()
        }
    }

    fun setRangeQuery(query: String, lower: Int, upper: Int) {
        queries[query] = "$lower..$upper"
    }

    override fun toString(): String {
        var query: String = pattern
        queries.forEach { (key, value) ->
            query += if (!query.isEmpty()) "+$key:$value" else "$key:$value"
        }
        return query
    }
}