package com.gabrielfv.desafioconcrete.api.models

import com.google.gson.annotations.SerializedName

data class ApiResponse(
        @SerializedName("total_count") val count: Int,
        @SerializedName("incomplete_results") val incompleteResults: Boolean,
        val items: List<Repo>
)