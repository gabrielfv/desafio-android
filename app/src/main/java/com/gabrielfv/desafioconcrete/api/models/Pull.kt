package com.gabrielfv.desafioconcrete.api.models

import com.google.gson.annotations.SerializedName
import java.util.*

data class Pull(
        val body: String,
        val user: User,
        val title: String,
        @SerializedName("created_at") val createdAt: Date,
        @SerializedName("updated_at") val updatedAt: Date)