package com.gabrielfv.desafioconcrete.api.di

import android.app.Application
import com.gabrielfv.desafioconcrete.api.Constants
import com.gabrielfv.desafioconcrete.api.GithubApi
import com.gabrielfv.desafioconcrete.api.GithubApiService
import com.gabrielfv.desafioconcrete.app.SchedulerProvider
import com.gabrielfv.desafioconcrete.app.di.ForApplication
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiModule() {
    @Provides
    @Singleton
    fun providesCache(app: Application): Cache = Cache(app.cacheDir, Constants.CACHE_SIZE)

    @Provides
    fun providesGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun providesOkHttpClient(cache: Cache): OkHttpClient = OkHttpClient.Builder()
            .cache(cache)
            .build()

    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(Constants.GITHUB_API_BASE_URL)
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    fun providesService(retrofit: Retrofit): GithubApiService = retrofit
            .create(GithubApiService::class.java)

    @Provides
    @ForApplication
    fun providesApi(
            service: GithubApiService,
            schedulerProvider: SchedulerProvider
    ): GithubApi = GithubApi(service, schedulerProvider)
}