package com.gabrielfv.desafioconcrete.api

import com.gabrielfv.desafioconcrete.api.models.Pull
import com.gabrielfv.desafioconcrete.api.models.Repo
import com.gabrielfv.desafioconcrete.app.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GithubApi @Inject constructor(
        private val service: GithubApiService,
        private val schedulerProvider: SchedulerProvider
) {
    class SortBy private constructor(private val sort: String) {
        companion object {
            val STARS = SortBy("stars")
            val FORKS = SortBy("forks")
            val UPDATED = SortBy("updated")
        }

        override fun toString(): String = sort
    }

    class OrderBy private constructor(private val order: String) {
        companion object {
            val DESC = OrderBy("desc")
            val ASC = OrderBy("asc")
        }

        override fun toString(): String = order
    }

    fun fetchRepos(
            query: GithubSearchQuery,
            order: OrderBy? = null,
            sortBy: SortBy? = null,
            page: Int? = null
    ): Observable<Repo>? = service
            .searchRepos(
                        query.toString(),
                        sortBy?.toString(),
                        order?.toString(),
                        page)
            ?.subscribeOn(schedulerProvider.io())
            ?.flatMap { apiResult -> Observable.fromArray(apiResult.items) }
            ?.flatMapIterable { it }

    fun fetchRepoPulls(repo: Repo, page: Int = 1): Observable<Pull>? {
        val (owner, name) = repo.fullName.split('/')
        return service.getRepoPulls(owner, name, page)
                ?.subscribeOn(Schedulers.io())
                ?.flatMapIterable { it }
    }
}