package com.gabrielfv.desafioconcrete.api.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
        val login: String,
        @SerializedName("avatar_url") val avatarUrl: String): Parcelable