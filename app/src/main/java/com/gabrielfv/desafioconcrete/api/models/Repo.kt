package com.gabrielfv.desafioconcrete.api.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Repo(
        val name: String,
        val owner: User,
        val description: String,
        @SerializedName("forks_count") val forksCount: Int,
        @SerializedName("full_name") val fullName: String,
        @SerializedName("stargazers_count") val stargazersCount: Int): Parcelable