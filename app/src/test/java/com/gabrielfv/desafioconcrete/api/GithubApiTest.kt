package com.gabrielfv.desafioconcrete.api

import com.gabrielfv.desafioconcrete.api.models.ApiResponse
import com.gabrielfv.desafioconcrete.api.models.Pull
import com.gabrielfv.desafioconcrete.api.models.Repo
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import java.util.concurrent.TimeUnit

class GithubApiTest {
    lateinit var api: GithubApi
    lateinit var mockGithubApiService: MockGithubApiService
    lateinit var repoResponse: List<Repo>
    lateinit var pullResponse: List<Pull>

    val networkBehavior = NetworkBehavior.create()

    inner class MockGithubApiService(
            private val delegate: BehaviorDelegate<GithubApiService>) : GithubApiService {
        override fun searchRepos(
                query: String,
                sortBy: String?,
                order: String?,
                page: Int?): Observable<ApiResponse>? {
            val response = mock(ApiResponse::class.java)
            `when`(response.items).thenReturn(repoResponse)
            `when`(response.count).thenReturn(repoResponse.size)

            return delegate.returningResponse(response).searchRepos(query, sortBy, order, page)
        }

        override fun getRepoPulls(
                owner: String,
                name: String,
                page: Int): Observable<List<Pull>>? {
            val response = pullResponse
            return delegate.returningResponse(response).getRepoPulls(owner, name, page)
        }
    }

    @Before
    fun setUp() {
        repoResponse = listOf(
                mock(Repo::class.java),
                mock(Repo::class.java)
        )

        pullResponse = listOf(
                mock(Pull::class.java),
                mock(Pull::class.java),
                mock(Pull::class.java)
        )

        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.GITHUB_API_BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        val mockRetrofit = MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build()

        mockGithubApiService = MockGithubApiService(
                mockRetrofit.create(GithubApiService::class.java))
    }

    @Test
    fun apiSearchCallReadsCorrectly() {
        givenNetworkFailurePercentIs(0)
        val observer: TestObserver<ApiResponse> = TestObserver()
        mockGithubApiService.searchRepos("")
                ?.subscribe(observer)

        observer.assertNoErrors()
        observer.assertValue { res ->
            res.items.size == repoResponse.size && res.items.all { item -> item in repoResponse }
        }
        observer.assertComplete()
    }

    @Test
    fun apiPullsCallReadsCorrectly() {
        givenNetworkFailurePercentIs(0)
        val observer: TestObserver<List<Pull>> = TestObserver()
        mockGithubApiService.getRepoPulls("", "")
                ?.subscribe(observer)

        observer.assertNoErrors()
        observer.assertValue { res ->
            res.size == pullResponse.size && res.all { item -> item in pullResponse }
        }
        observer.assertComplete()

    }

    private fun givenNetworkFailurePercentIs(pct: Int) {
        networkBehavior.setDelay(0, TimeUnit.MILLISECONDS)
        networkBehavior.setVariancePercent(0)
        networkBehavior.setFailurePercent(pct)
    }
}
