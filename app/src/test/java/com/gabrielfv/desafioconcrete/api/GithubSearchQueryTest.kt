package com.gabrielfv.desafioconcrete.api

import org.junit.Assert.assertEquals
import org.junit.Test

class GithubSearchQueryTest {

    @Test
    fun queryTranslatesCorrectly() {
        val query = GithubSearchQuery.Builder("android")
                .setQuery("language", "kotlin")
                .setRangeQuery("stars", 10, 500)
                .setRelationQuery("topics", 20,
                        GithubSearchQuery.Relation.LOWER_THEN or
                                GithubSearchQuery.Relation.EQUAL)
                .setQuery("user", "gfreivasc", true)
                .build()

        var cases = 5
        query.toString().split("+").map { it.split(":") }.forEach {
            when (it[0]) {
                "language" -> assertEquals("kotlin", it[1])
                "stars" -> assertEquals("10..500", it[1])
                "topics" -> assertEquals("<=20", it[1])
                "-user" -> assertEquals("gfreivasc", it[1])
                "android" -> null
                else -> throw Exception("Unexpected param ${it[0]}")
            }
            cases--
        }
        assertEquals(0, cases)
    }

    @Test
    fun queryRelationsTranslateCorrectly() {
        val query = GithubSearchQuery.Builder()
                .setRelationQuery("stars", 50, GithubSearchQuery.Relation.GREATER_THEN)
                .setRelationQuery("topics", 0)
                .setRelationQuery("forks", 1000,
                        GithubSearchQuery.Relation.LOWER_THEN or
                                GithubSearchQuery.Relation.EQUAL)
                .setRelationQuery("size", 2048, GithubSearchQuery.Relation.LOWER_THEN)
                .setRelationQuery("notget", 20,
                        GithubSearchQuery.Relation.GREATER_THEN or
                                GithubSearchQuery.Relation.EQUAL)
                .build()

        var cases = 5
        query.toString().split("+").map { it.split(":") }.forEach {
            cases--
            when (it[0]) {
                "stars" -> assertEquals(">50", it[1])
                "topics" -> assertEquals("0", it[1])
                "forks" -> assertEquals("<=1000", it[1])
                "size" -> assertEquals("<2048", it[1])
                "notget" -> assertEquals(">=20", it[1])
                else -> throw Exception("Unexpected param ${it[0]}")
            }
        }
        assertEquals(0, cases)
    }

    @Test(expected = IllegalArgumentException::class)
    fun queryCannotRelateLowerAndGreater() {
        GithubSearchQuery.Builder()
                .setRelationQuery("stars", 40,
                        GithubSearchQuery.Relation.GREATER_THEN or
                                GithubSearchQuery.Relation.LOWER_THEN)
    }
}