package com.gabrielfv.desafioconcrete.ui

import com.gabrielfv.desafioconcrete.TestSchedulerProvider
import com.gabrielfv.desafioconcrete.api.GithubApi
import com.gabrielfv.desafioconcrete.api.models.Repo
import com.gabrielfv.desafioconcrete.app.SchedulerProvider
import com.gabrielfv.desafioconcrete.ui.repos.RepoViewModel
import com.nhaarman.mockito_kotlin.any
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import java.net.ConnectException

class RepoViewModelTest {
    @Mock
    private lateinit var githubApi: GithubApi

    @Spy
    val schedulerProvider: SchedulerProvider = TestSchedulerProvider()

    @InjectMocks
    private lateinit var viewModel: RepoViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    private fun matchedStubbing() = `when`(githubApi.fetchRepos(any(), any(), any(), any()))

    @Test
    fun dataIsFetchedUponSubscription() {
        matchedStubbing().thenReturn(Observable.just(mock(Repo::class.java)))

        assertTrue(viewModel.data.isEmpty())
        viewModel.subscribe({})
        assertEquals(1, viewModel.data.size)
    }

    @Test
    fun subscriberNotifiedOnDataSetUpdated() {
        matchedStubbing().thenReturn(Observable.just(mock(Repo::class.java)))

        assertEquals(0, viewModel.data.size)
        assertEquals(1, viewModel.page)

        viewModel.subscribe({
            assertEquals(1, viewModel.data.size)
            assertEquals(2, viewModel.page)
        }, {
            throw it
        })
    }

    @Test
    fun viewModelNotifiesOnError() {
        matchedStubbing().thenReturn(Observable.error(ConnectException("Test")))

        viewModel.subscribe({
            throw IllegalAccessError()
        }, { e ->
            assertEquals("Test", e.message)
            assertTrue(e is ConnectException)
        })
    }

    @Test
    fun viewModelNotifiesNoDataErrorWhenNoDataFound() {
        matchedStubbing().thenReturn(Observable.empty())

        viewModel.subscribe({}, {
            assertTrue(it is BaseViewModel.NoDataFetchedException)
        })
    }

    @Test
    fun viewModelNotifiesAllDataHasBeenFetched() {
        matchedStubbing().thenReturn(Observable.just(mock(Repo::class.java)))

        var called = false
        viewModel.subscribe({}, {}, {
            called = true
        })
        assertTrue(called)
    }
}