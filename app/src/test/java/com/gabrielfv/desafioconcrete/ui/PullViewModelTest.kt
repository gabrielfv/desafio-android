package com.gabrielfv.desafioconcrete.ui

import com.gabrielfv.desafioconcrete.TestSchedulerProvider
import com.gabrielfv.desafioconcrete.api.GithubApi
import com.gabrielfv.desafioconcrete.api.models.Pull
import com.gabrielfv.desafioconcrete.api.models.Repo
import com.gabrielfv.desafioconcrete.app.SchedulerProvider
import com.gabrielfv.desafioconcrete.ui.pulls.PullViewModel
import com.nhaarman.mockito_kotlin.any
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.*
import java.net.ConnectException

class PullViewModelTest {
    @Mock
    private lateinit var githubApi: GithubApi

    @Spy
    val schedulerProvider: SchedulerProvider = TestSchedulerProvider()

    @InjectMocks
    private lateinit var viewModel: PullViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel.init(Mockito.mock(Repo::class.java))
    }

    private fun matchedStubbing() = Mockito.`when`(githubApi.fetchRepoPulls(any(), any()))

    @Test
    fun dataIsFetchedUponSubscription() {
        matchedStubbing().thenReturn(Observable.just(Mockito.mock(Pull::class.java)))

        Assert.assertTrue(viewModel.data.isEmpty())
        viewModel.subscribe({})
        Assert.assertEquals(1, viewModel.data.size)
    }

    @Test
    fun subscriberNotifiedOnDataSetUpdated() {
        matchedStubbing().thenReturn(Observable.just(Mockito.mock(Pull::class.java)))

        Assert.assertEquals(0, viewModel.data.size)
        Assert.assertEquals(1, viewModel.page)

        viewModel.subscribe({
            Assert.assertEquals(1, viewModel.data.size)
            Assert.assertEquals(2, viewModel.page)
        }, {
            throw it
        })
    }

    @Test
    fun viewModelNotifiesOnError() {
        matchedStubbing().thenReturn(Observable.error(ConnectException("Test")))

        viewModel.subscribe({
            throw IllegalAccessError()
        }, { e ->
            Assert.assertEquals("Test", e.message)
            Assert.assertTrue(e is ConnectException)
        })
    }

    @Test
    fun viewModelNotifiesNoDataErrorWhenNoDataFound() {
        matchedStubbing().thenReturn(Observable.empty())

        viewModel.subscribe({}, {
            Assert.assertTrue(it is BaseViewModel.NoDataFetchedException)
        })
    }

    @Test
    fun viewModelNotifiesAllDataHasBeenFetched() {
        matchedStubbing().thenReturn(Observable.just(Mockito.mock(Pull::class.java)))

        var called = false
        viewModel.subscribe({}, {}, {
            called = true
        })
        Assert.assertTrue(called)
    }
}
